var fs = require("fs");
var access = fs.createWriteStream('output.txt')
var request = require("request")
var keyword_extractor = require("keyword-extractor");
var servercount = 1;
var url = "http://playrust.io/servers.json";

var keywords = ["tp",
                "10k",
                "x2","x3","x4","x5","x6","x7","x8","x9","x10","x11","x12","x13","x14","x15",
                "kit"];

process.stdout.write = process.stderr.write = access.write.bind(access);

process.on('uncaughtException', function(err) {
  console.error((err && err.stack) ? err.stack : err);
});

request({url: url, json: true}, function (error, response, body) {

    if (!error && response.statusCode === 200) {

        for(var item of body) {
          if (item.modded != 1) {
            //console.log(item);
            var checkhostname = true;

            if(checkhostname) {
              var extraction_result = keyword_extractor.extract(item.hostname,{
                language:"english",
                remove_digits: true,
                return_changed_case:true,
                remove_duplicates: true
              });

              var ext_res = extraction_result;

              for (ki = 0; ki < keywords.length; ++ki) {
                for(ei = 0; ei < ext_res.length; ++ei) {
                  if(ext_res[ei] == keywords[ki]) {
                    console.log('[WARNING COULD BE DUPLICATED OR ALLOWED SO DOUBLE CHECK]')
                    //console.log(item.hostname)
                    console.log('Server #:', servercount);
                    console.log('Server IP: ', item.endpoint);
                    console.log('Country: ', item.country);
                    console.log('Hostname: ', item.hostname);
                    //console.log('Map: ', item.level);
                    console.log('Players: ', item.players, '/', item.maxplayers);
                    //console.log('Version: ', item.version);
                    //console.log('OS: ', item.os);
                    console.log('Non-Modded Tags: ', item.tags);
                    console.log('World-Size: ', item.world);
                    //console.log('FPS: ', item.fps);
                    console.log('LastUpdated: ', item.updatetime);
                    console.log('----------------------------------------------');
                    console.log('----------------------------------------------');
                    servercount++;
                  }
                }
              }

        }

    }
  }
}
});
